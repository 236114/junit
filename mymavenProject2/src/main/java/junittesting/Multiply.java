package junittesting;

public class Multiply {
	public int multi(int x, int y) {
		
		return x*y;
	}
	
	public double avg(double a, double b, double c) {
		 return (a+b+c)/3;
	}
	
	public String percentageA(double a, double b) {
		String p = (a/b)*100+"";
		return p;
	}
	
	public static int avg2(int a, int b, int c, int d) {
		return (a+b+c+d)/4;
	}
	
	public char character(char a) {
		return a;
	}
	
	
}
