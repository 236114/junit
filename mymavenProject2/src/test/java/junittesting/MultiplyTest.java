package junittesting;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class MultiplyTest {
	Multiply m = new Multiply();
	@Test
	void test() {
		
		assertEquals(30,m.multi(5, 6));//assertEquals(expected value, output value)
		
	}
	@BeforeEach
	void testAvg() {
		assertEquals(3,m.avg(2, 3, 4));//we can see the error information in the failure trace section
	}//but already change to no error
	
	@AfterEach
	void testPercentageA() {
		assertEquals("10.0",m.percentageA(1, 10));//because the method is in String
	}
	
	@BeforeAll
	static void beforeall() {
		System.out.println("before all");
	}
	
	@AfterAll
	static void afterall() {
		System.out.println("after all");
	}
	
	@Test
	void testcharacter() {
		assertEquals('b', m.character('b'));
	}
	
}
