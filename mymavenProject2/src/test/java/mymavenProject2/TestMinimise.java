package mymavenProject2;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import junittesting.Minimise;


public class TestMinimise {
	
	@Test
	public void testMinimise(){
		Minimise mn = new Minimise();
		assertEquals(5, mn.minimise(10, 5));
	}
	

}
